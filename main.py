#!python3

import random
import os
import sys
from time import strftime
from subprocess import Popen, CREATE_NEW_CONSOLE
import ctypes

ctypes.windll.kernel32.SetConsoleTitleA(b"Fork-Handle")

class Main(object):
    def __init__(self):
        self.args = sys.argv
        self.command = []
        if self.args:
            self.command = self.args[1:]
        self.logfile = 'userlog.log'
        self.commandlist = {"say": self.hello,
                            "help": self.guide,
                            "log": self.log,
                            "clear": self.consoleclear,
                            "clipboard": self.extendedclipboard,
                            "calculator": self.copycalc}
        self.helpmsg = {"say": "Prints anything that comes after the command.",
                        "help": "Prints this command reference manual. Duh! :p",
                        "log": "Write on a log file.\n   Accepted arguments: read, write, prune",
                        "clear": "Clear the window",
                        "clipboard": "Record all copied text into a log file",
                        "calculator": "Copy a math expression and get the answer ready to be pasted",
                        "exit": "Close the program. Using 'quit' works too!"}

    def startup(self):
        message = """
    ###############################
    #    Fork-Handle; Welcome!    #
    ###############################
    Type 'help' to see a list of commands.
    """
        print(message)

    def argparse(self, limit=1):
        args = self.command
        del args[0:limit]
        string = " ".join(args)
        string.strip()
        return string

    def argcount(self, command):
        return len(command)-1

    def hello(self):
        word = self.argparse()
        if not word:
            print("Are you alright, sir?")
        else:
            print(word)

    def log(self):
        args = self.argparse()
        if not args:
            print('Not enough arguments!')
            return
        args = args.split(' ')
        if 'write' == args[0]:
            self.logwrite(args)
        elif 'read' == args[0]:
            self.logprint()
        elif 'prune' == args[0]:
            self.logclear()
        else:
            raise ValueError("Command 'log' has no argument called {0}!".format(args[0]))

    def logwrite(self, data):
        with open(self.logfile, 'a') as f:
            data.pop(0)
            if data:
                inputdata = " ".join(data)
            else:
                inputdata = input('Log: ').strip()
            if inputdata:
                f.write(strftime('%Y/%m/%d, %H:%M:%S; ') + inputdata + "\n")
            else:
                print("Input cancelled.")

    def logprint(self):
        try:
            with open(self.logfile) as f:
                print(f.read())
        except FileNotFoundError:
            with open(self.logfile, 'a') as f:
                print('New logfile created.')

    def logclear(self):
        with open(self.logfile, 'w') as f:
            f.write("")

    def consoleclear(self):
        os.system('cls' if os.name == 'nt' else 'clear')
        self.startup()

    def externalsoftware(self, file):
        Popen([sys.executable, file], creationflags=CREATE_NEW_CONSOLE)

    def extendedclipboard(self):
        try:
            self.externalsoftware('xtendClipboard.py')
        except FileNotFoundError:
            print("Requsted script was not found.")

    def copycalc(self):
        try:
            self.externalsoftware('PyCalc.py')
        except FileNotFoundError:
            print('Requested script was not found.')

    def guide(self):
        cmd = self.argparse()
        if not cmd:
            for key in self.helpmsg:
                print("// {0}: {1}".format(key, self.helpmsg[key]))
        else:
            cmd = cmd.split()[0]
            if cmd in self.helpmsg.keys():
                print('// {0}: {1}'.format(cmd, self.helpmsg[cmd]))
            else:
                print("Error: No such command found!")

    def run(self, args=False):
        if args:
            self.commandlist[args[1].lower()]()
            exit()
        else:
            self.startup()
            while True:
                self.command = input(">_ ").split()
                arg = self.command[0].lower()
                for keyword in self.commandlist.keys():
                    if arg == keyword:
                        self.commandlist[arg]()
                        break
                    elif arg == 'exit' or arg == 'quit':
                        sys.exit('Thank you, come again!')
                    elif arg not in self.commandlist:
                        print("Command not found!")
                        break
        sys.exit()

def main():
    fk = Main()
    if len(fk.args) > 1:
        fk.run(fk.args)
    else:
        fk.run()

if __name__ == "__main__":
    main()
