#Using the Fork-Handle toolkit
Fork-Handle is a collection of Python programs. It's quite easy to modify if you would like to add support for your own scripts.

##Usage
###1. Executing the program normally
  1. Run main.py, either through a shell or by double-clicking the script file
  2. As the program starts, you should see a shell with the name 'Fork-Handle'
  3. To run a sub-program, type its name and possible arguments
  4. For a list of all sub-programs currently available, type "help" in the program shell
###2. Executing sub-programs through command-line arguments
  1. Open a shell in the script directory
  2. Type "main.py [sub-program] [*arguments]"

As of now, all sub-programs can be ran without running the full program.

Examples:

1. To run the logging sub-program, write 'Hello, World!' and read the log file:
  1. Run main.py
  2. In the Fork-Handle shell, type "log write Hello, World!"
  3. In the Fork-Handle shell, type "log read"
2. To run the calculator script as an argument:
  1. Open a shell in the script directory
  2. Type "main.py calculator"
  
