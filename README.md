# fork-handle
A collection of more or less useful, individual Python tools for office work and file handling

The goal of the project is to serve as an all-in-one pack of tools anyone can use either individually or through the hub-tool. Everything is open-source.

## Changelog:
* 20.11.2016 - Re-coded the main script. It now supports arguments and uses classes!
* 10.8.2016 - Added the PyCalc-script, a copy-paste math calculator

