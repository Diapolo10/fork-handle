#! python3
# Log all copied items.
from sys import executable
from subprocess import Popen, CREATE_NEW_CONSOLE
import pyperclip
import time
import datetime
import ctypes
ctypes.windll.kernel32.SetConsoleTitleA(b"Extended Clipboard")

def appendlog(string):
    try:
        f = open("clipboard.log", "a")
        date = datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S")
        f.write("### " + str(date) + " ###" + ":\n" + string + "\n")
    except FileNotFoundError:
        print("Error: File not found!")
    finally:
        f.close()

#if __name__ == "__main__":
print("Extended Clipboard")
previous = ""
while True:
    pasteitem = pyperclip.paste()
    if pasteitem != previous and previous != "":
        previous = pasteitem
        appendlog(previous)
    elif pasteitem != previous and previous == "": # 
        previous = pasteitem
    time.sleep(0.1)
        
        
    
