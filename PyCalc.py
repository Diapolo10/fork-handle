#!python3
import pyperclip
import time
import ctypes
import sympy

ctypes.windll.kernel32.SetConsoleTitleA(b"Copypaste calculator")

mathproof = ["+", "-", "*", "/", "="]
parsedict = {"×": "*",
             "÷": "/",
             "^": "**"}

def stringparse(string):
    string = string.replace(" ", "")
    for i in string:
        if i in parsedict.keys():
            string = string.replace(i, parsedict[i])
    print(string)
    return string

def solver(string_exp):
    #string_exp = stringparse(string_exp)
    x = sympy.Symbol('x')
    y = sympy.S(string_exp)
    y = sympy.simplify(y)
    print(y)
    z = sympy.solve(y)
    if len(z) > 1:
            z = [str(i) for i in z]
            pyperclip.copy(", ".join([str(i) for i in z]))
            return "Possible solutions: x={0} or {1} when y=0.".format(", ".join(z[0:-1]), z[-1])
    else:
            pyperclip.copy(str(z[0]))
            return "Solution: x is {0}.".format(z[0])

def init():
    current = pyperclip.paste()
    new = current
    main(current, new)

def main(current, new):
    print("Calculate the answer of a copied symbolic expression, \nand paste it on the clipboard.")
    while True:
        new = pyperclip.paste()
        if new != current:
            print(solver(new))
            pass
        else:
            time.sleep(1.0)
            pass
        current = new

if __name__ == "__main__":
    init()