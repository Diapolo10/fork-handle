#!python3

import re
import pyperclip
import time
import ctypes
import sympy

ctypes.windll.kernel32.SetConsoleTitleA(b"The lazy fella's calculator")

class Main(object):
    def __init__(self):
        self.mathproof = ["+", "-", "*", "/", "="]
        self.parsedict = {"×": "*",
                          "÷": "/",
                          "^": "**"}
        self.current = pyperclip.paste()
        self.new = self.current

    def is_math(self, string):
        return any([True for item in string if item in self.mathproof])

    def stringparse(self, string):
        line = []
        for character in string:
            if character in self.parsedict.keys():
                line.append(self.parsedict[character])
            else:
                line.append(character)
        output = "".join(line)
        print(output)
        return output

    def solver(self, string):
        string_exp = self.stringparse(string)
        x = sympy.Symbol('x')
        y = sympy.S(string_exp)
        y = sympy.simplify(y)
        print(y)
        z = sympy.solve(y)
        if len(z) > 1:
            z = [str(i) for i in z]
            pyperclip.copy(", ".join(z))
            return "Possible solutions: x is {0} or {1} when y=0.".format(", ".join(z[0:-1]), z[-1])
        else:
            pyperclip.copy(str(z[0]))
            return "Solution: x is {0}.".format(z[0])

    def run(self):
        print("Calculate the answer of  copied symbolic expression,\nand paste it on the clipboard.")
        while True:
            self.current = self.new
            self.new = pyperclip.paste()
            if self.new != self.current:
                if self.is_math(self.new):
                    print(self.solver(self.new))
                    pass
                pass
            else:
                time.sleep(1.0)
                pass

def main():
    instance = Main()
    instance.run()

if __name__ == "__main__":
    main()
